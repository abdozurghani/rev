<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>

<body>
    <?php
    // include database and class files
    include_once '../config/database.php';
    include_once '../class/Register.php';
    //check if passwords match
    if(isset($_POST['password'])&&$_POST['password']!=$_POST['repeat_password']){
        echo "Passwords Do Not Match!<br>";
    }
    //register user
    if (isset($_POST['email']) && $_POST['password']==$_POST['repeat_password']) {
        $database = new Database();
        $db = $database->getConnection();
        $registeration = new Register($db,$_POST['name'],$_POST['email'],$_POST['password']);
        
        //check if email already registered
        if($registeration->emailDoseExist()){
            echo"Email Already Registered On The System";
        }else{
            $registeration->addEmail();
            header( "Location: login.php?massege=Registered Successfully.<br> You Can Now Login" );
        };
    }
    

    ?>
    <form method="post" action="registeration.php">
        <h1>Register</h1>
        <p>Please fill in this form to create an account.</p>
        <hr>
        <label for="name"><b>Name --------------</b></label>
        <input type="text" placeholder="Enter Name" name="name" required>
        <br>
        <label for="email"><b>Email --------------</b></label>
        <input type="text" placeholder="Enter Email" name="email" required>
        <br>
        <label for="password"><b>Password ---------</b></label>
        <input type="password" placeholder="Enter Password" name="password" required>
        <br>
        <label for="repeat_password"><b>Repeat Password</b></label>
        <input type="password" placeholder="Repeat Password" name="repeat_password" required>
        <br>
        <button type="submit">Register</button>
        <hr>
    </form>
</body>

</html>