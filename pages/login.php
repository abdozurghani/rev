<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>

<body>
    <?php
    // include database and class files
    include_once '../config/database.php';
    include_once '../class/Register.php';
    //show massege from redirection
    if (isset($_GET['massege'])) {
        print_r($_GET['massege']);
    }
    //use user input to try to login
    if (isset($_POST['email'])) {
        $database = new Database();
        $db = $database->getConnection();
        $credentials = new Register($db,"",$_POST['email'],$_POST['password']);
        $credentials->login();
    }


    ?>
    <form method="post" action="login.php">
        <h1>Login</h1>
        <p>Please fill in this form to login into your account.</p>
        <hr>

        <label for="email"><b>Email --------------</b></label>
        <input type="text" placeholder="Enter Email" name="email" required>
        <br>
        <label for="password"><b>Password ---------</b></label>
        <input type="password" placeholder="Enter Password" name="password" required>
        <br>

        <button type="submit">login</button>
        <hr>
    </form>
</body>

</html>